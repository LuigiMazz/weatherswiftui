//
//  ContentView.swift
//  weatherSwiftUI
//
//  Created by Luigi Mazzarella on 24/03/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var weather = LocationManager()
    
    var body: some View {
        VStack{
            Text("\(weather.current?.name ?? "Milano")")
            
            Text("\(weather.current?.weather[0].weatherDescription ?? "Niente")")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
