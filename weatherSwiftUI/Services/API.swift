//
//  API.swift
//  weatherSwiftUI
//
//  Created by Luigi Mazzarella on 24/03/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import Foundation
import SwiftUI

class HTTPManager {
    static let shared = HTTPManager()
    let apiKey = "afd0e7890f98c3e219dd9520b1722ea9"
    
    //Tipi di closures
    typealias HTTPManagerResponse = (Bool,Any?) -> Void //Params: Success,Data/Error
    typealias HTTPWeatherDataResponse = (Bool,CurrentWeatherAPI?) -> Void //Parmas: Success,Data/Error
    func executeRequest(request: URLRequest, completion: @escaping HTTPManagerResponse) {
        
        var req = request
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task = session.dataTask(with: req) { (data, response, error) in
            guard error == nil else {
                completion(false,error)
                return
            }
            
            let statusCode = (response != nil && response is HTTPURLResponse) ? (response as! HTTPURLResponse).statusCode : 400
            guard statusCode >= 200 && statusCode < 300 else {
                completion(false,data)
                return
            }
            
            completion(true,data)
        }
        task.resume()
    }
    
    func downloadWeatherData(lat: Double, lon: Double, completion: @escaping HTTPWeatherDataResponse) {
        let endpoint = "https://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&units=metric&appid=\(apiKey)&lang=it"
        print(endpoint)
        guard let endpointURL = URL(string: endpoint) else {
            print("The URL is invalid")
            return
        }
        
        var request = URLRequest(url: endpointURL)
        request.httpMethod = "GET"
        
        HTTPManager.shared.executeRequest(request: request) { (success, data) in
            guard success else {
                DispatchQueue.main.async {
                    completion(false,nil)
                }
                return
            }
            
            guard data != nil && data is Data else {
                DispatchQueue.main.async {
                    completion(false,nil)
                }
                return
            }
            
            let decoder = JSONDecoder()
            do {
                let currentWeather = try decoder.decode(CurrentWeatherAPI.self, from: data as! Data)
                DispatchQueue.main.async {
                    completion(true, currentWeather)
                }
            } catch {
                print(error)
                DispatchQueue.main.async {
                    completion(false, nil)
                }
            }
            
        }
        
    }
}
